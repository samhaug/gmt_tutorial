#!/bin/bash

##################################################
# Make some kind of topography as a function of lat/lon
# with a C code. Compile and execute it here. The resulting
# topography file is three columns (lon/lat/value)
# and is called topo_data.dat
##################################################

gcc datfiles/make_topo.c -lm -o datfiles/make_topo
(cd datfiles && ./make_topo)

##################################################

region=70/180/-30/60 
proj=L130/15/-30/60/4i
scale=M4i
file=harmonic_map

#Make cmap.cpt file by giving colormap name, and 
# -Ccolormapname look up online for different names
#  Or type 'makecpt' in the command line.
# -T sets the min/max/increment
# -I inverts the colormap
# -Z makes the colormap continuous (default is discrete)
makecpt -Cpolar -T-1/1/0.1 -I -Z > cmap.cpt

# Have to make a grid file from the topo data.
# xyz2grid turns the ascii file to tmp.grd, which grdimage plots.
# -I indicates x/y grid spacing. You can specify in units or not.
xyz2grd datfiles/topo_data.dat -R$region -Gtmp.grd -I1/1

# Plot the tmp.grid file made from xyz2grd and specify the cpt file
# made with makecpt
grdimage tmp.grd -R$region -J$proj -Ba20 -BENsw -K -Ccmap.cpt -P -Y4i > $file.ps

# draw coastlines
pscoast -R$region -J$proj -W1,black -A1000 -K -O -P >> $file.ps

# Make colormap with the same cpt file
psscale -Ccmap.cpt -Dx0c/-2c+w9.2c/0.5c+h -Bxaf+l"Topography (km)" -K -O -P >> $file.ps

#clean up
rm cmap.cpt
rm tmp.grd
rm gmt.history
evince $file.ps
