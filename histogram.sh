#!/bin/bash
#########################################
#plot regular and cumulative histogram
#########################################

plotname=histogram
file=datfiles/histogram_data

#xmin/xmax/ymin/ymax
reg=-R0/250/0/100
#2.5 inch scale
proj=-JX2.5i
#set tickmarks and axes labels
bord=-Ba50f5:"Distance\040(km)":/a25f5:"Fraction":WS
w=10

# -W is bin width. Make into variable for easy changing
# -S is stair-step. If not included there will be internal vertical bars

# -Z is number between 0 and 5.
# -Z=0 is counts [Default]
# -Z=1 is frequency percent
# -Z=2 is log(1.0 + count)
# -Z=3 is log(1.0 + frequency_percent)
# -Z=4 is log10(1.0 + count)
# -Z=5 is log10(1.0 + frequency_percent) 

# -L specifies pen attributes. See website for more
# https://www-k12.atmos.washington.edu/~ovens/gmt/doc/html/GMT_Docs/node49.html

# -P for portrait mode

pshistogram $file $bord $proj $reg -W$w -L3p,red -S -Z1 -P -K > $plotname.ps
# -Q draws cumulative histogram
# -X shifts plot horizontally by 3 inches from last plot
pshistogram $file $bord $proj $reg -W$w -L1p,blue -Z1 -Q -X3i -O -P >> $plotname.ps

evince $plotname.ps
