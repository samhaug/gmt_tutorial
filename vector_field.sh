#!/bin/bash

################################################
# Use awk to make a simple 2D vector field.
# Then use psvelo to plot it.
################################################

awk -v seed=$RANDOM 'BEGIN{srand(seed);
                     for(i=0;i<=1;i+=0.1)
                     for(j=0;j<=1;j+=0.1)
                     print i,j,-i+cos(j),sin(i)+j*j*rand(),rand()/10,rand()/10}' > vector.dat 

reg=0/1/0/1
proj=X4.0i


gmt psvelo vector.dat -R$reg -J$proj -Ba0.25f0.05 -Sr2/0.68/12p\
                      -BWSne\
                      -W1p,blue -Gdarkgray > vector.ps

ps2pdf vector.ps  
evince vector.pdf 
