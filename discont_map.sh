#!/bin/bash
region=90/150/-10/50 
boxregion=70/180/-30/60 
scale=m0.06i
file=synth_topo
perspective=140/20

#Map syntetic topography with map.xyz

echo "makecpt"
makecpt -Ccork.cpt -T-15/15/1.0 > cmap.cpt
echo "xyz2grd"
xyz2grd map.xyz -R$region -Gtmp.grd -I1.0/1.0
xyz2grd map_neg.xyz -R$region -Gtmp_neg.grd -I1.0/1.0
echo "grdimage"
psscale -Ccmap.cpt -Dx0c/-2c+w9.2c/0.5c+h -Bxaf+l"Topography (km)" -Y5 -X4c -K -P > $file.ps
grdview tmp.grd -R$region -J$scale -JZ0.5i -Y1c -X-2c -p$perspective -K -O >> $file.ps
#grdimage tmp.grd -R$region -J$scale -Ccmap.cpt -p120/30 -K -O >> $file.ps
grdview tmp_neg.grd -R$region -J$scale -JZ0.5i -Y5c -p$perspective -K -O >> $file.ps
grdimage tmp.grd -R$region -JB120/-10/20/40/4.0i -Ba10 -Ccmap.cpt -Y7c -K -O >> $file.ps
pscoast -R$region -JB120/-10/20/40/4.0i -W1,black -A1000 -K -O >> $file.ps



ps2pdf12 $file.ps &> /dev/null
convert -density 150 $file.pdf -quality 90 $file.png &> /dev/null
rm {g,m,}tmp.grd &> /dev/null
rm {g,m,}cmap.cpt &> /dev/null
rm $file.ps
evince $file.pdf &> /dev/null
