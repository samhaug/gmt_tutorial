#!/bin/bash
# Plot raypaths for sScS reverberations
file=ScSScS_raypaths
#Polar region size 5 inches
scale=P5i
#50 degrees to 130 degrees section off the mantle
region=50/130/3480/6371

# Plot the raypath as a thin red dotted line
psxy datfiles/ScSScS.dat -R$region -J$scale -Bxa10 -Bya1000 -BWNse \
                         -Wthin,red,.. -K -P > $file.ps

psxy datfiles/670.dat -R$region -J$scale \
                      -Wthinnest,black,- -K -O >> $file.ps

#Pipe the first line of the raypath file and use G to make it a polygon with fill
head -n1 datfiles/ScSScS.dat | gmt psxy  -R$region -J$scale -O -K -Sa.7 \
                                      -Wthin,black -Gyellow >> $file.ps
#############################################################################

evince $file.ps

